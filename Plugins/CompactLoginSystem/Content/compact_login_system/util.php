<?php

  // Authority code
  function make_authcode( $length = 6 )
  {
      $chars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 
      'i', 'j', 'k', 'l','m', 'n', 'o', 'p', 'q', 'r', 's', 
      't', 'u', 'v', 'w', 'x', 'y','z', 'A', 'B', 'C', 'D', 
      'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L','M', 'N', 'O', 
      'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y','Z', 
      '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', 
      '@','#', '+', '-', '*');

      $keys = array_rand($chars, $length); 
      $authcode = '';
      for($i = 0; $i < $length; $i++)
      {
          $authcode .= $chars[$keys[$i]];
      }
      return $authcode;
  }

  // Send auth code
  function send_authcode( $email, $aucode )
  {
        $to = $email;

        $msg = 'Hi, your authorization code is ' . $aucode . ', please use this to finish sign up.';

        $subject = 'Email Verification';
       
         //linux sever
        $cmd = 'echo "' . $msg . '" | mail -s "'.$subject.'" '.$to;
        exec($cmd, $output, $retval);

        return $retval;
  }

    // Client request response struct
    class ClientReqResp 
    {
        public $retcode = 0;
        public $message = '';
    }

?>